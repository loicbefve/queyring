import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TransactionsListComponent} from './transactionsList/transactions-list.component';
import {SingleTransactionComponent} from './singleTransaction/single-transaction.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SharedModule} from '../shared/shared.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    TransactionsListComponent,
    SingleTransactionComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SharedModule,
    BrowserAnimationsModule

  ],
  exports: [
    TransactionsListComponent,
    CommonModule,
    FormsModule
  ]
})
export class TransactionsModule {
}

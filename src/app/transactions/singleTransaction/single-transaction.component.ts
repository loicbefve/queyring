import {Component, Input, OnInit} from '@angular/core';
import {TransactionModel} from '../../models/transaction.model';
import {TransactionsService} from '../../services/transactions/transactions.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-single-transaction',
  templateUrl: './single-transaction.component.html',
  styleUrls: ['./single-transaction.component.scss']
})
export class SingleTransactionComponent implements OnInit {

  @Input() transaction: TransactionModel;
  @Input() index: number;

  constructor(
      private transactionsService: TransactionsService,
      private router: Router
  ) {
  }

  ngOnInit() {
  }

  onPaymentClick() {
    this.router.navigateByUrl('payment', {state: this.transaction});
  }

  onRecoveryClick() {
    this.transactionsService.updateState(this.transaction._id, 'VALIDATE');
  }

}

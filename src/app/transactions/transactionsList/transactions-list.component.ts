import {Component, OnDestroy, OnInit} from '@angular/core';
import {TransactionModel} from '../../models/transaction.model';
import {TransactionsService} from '../../services/transactions/transactions.service';
import {UserService} from '../../services/user/user.service';

@Component({
  selector: 'app-transactions-list',
  templateUrl: './transactions-list.component.html',
  styleUrls: ['./transactions-list.component.scss']
})
export class TransactionsListComponent implements OnInit, OnDestroy {

  listTransactions: TransactionModel[] = [];
  listTransactionsSubscription = this.transactionsService
      .transactionsListSubject
      .subscribe(
          (nextTransactionsList: TransactionModel[]) => {
            this.listTransactions = nextTransactionsList;
          }
      );

  constructor(
      private transactionsService: TransactionsService,
      private userService: UserService
  ) { }

  ngOnInit() {
    this.transactionsService.getUserTransactions(this.userService.getConnectedUserId());
  }

  ngOnDestroy(): void {
    this.listTransactionsSubscription.unsubscribe();
  }

}

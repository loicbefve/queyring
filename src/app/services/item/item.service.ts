import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AuthService} from '../auth/auth.service';
import {Subject, throwError} from 'rxjs';
import {ItemModel} from '../../models/item.model';
import {catchError, map} from 'rxjs/operators';
import {UserService} from '../user/user.service';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})

export class ItemService {

  itemList: ItemModel[];
  itemListSubject: Subject<ItemModel[]> = new Subject<ItemModel[]>();

  constructor(
      private httpClient: HttpClient,
      private authService: AuthService,
      private userService: UserService,
      private router: Router
  ) {
  }

  emitItemListSubject() {
    this.itemListSubject.next(this.itemList);
  }

  /**
   * Save a given item into the database into the correct user.
   * The user is retrieved on the API side through the token used
   * to send the request
   * @param item: The item to save
   */
  postItem(item: ItemModel): void {
    this.httpClient
    // Post datas
        .post(`${this.authService.apiUri}/items`,
            item,
            {
              headers:
                  {
                    Authorization:
                        `Bearer ${this.authService.getToken()}`
                  }
            }
        )
        .subscribe(
            resItem => {
              // Log that success
              console.log('The following item was successfully added:');
              console.log(resItem);
              // Navigate to the list object component:
              //  the list item component will call the getItems method
              //  and so update the itemList by itself
              this.router.navigate(['profile', 'items']);
            },
            error1 => {
                console.error('Error encountered when trying to add item');
                console.error(error1);
            }
        );

  }

  /**
   * Get the entire list of items for the logged user
   */
  getItems(): ItemModel[] {
    const userId = this.userService.getConnectedUserId();
    this.httpClient
        .get<ItemModel[]>(`${this.authService.apiUri}/users/${userId}/items`,
            {
              headers:
                  {
                    Authorization:
                        `Bearer ${this.authService.getToken()}`
                  }
            })
        // Transform the response into an ItemModel[]
        .pipe(
            map((items) => items.map((item: ItemModel) => new ItemModel(item))),
            catchError((err, caught) => throwError(err))
        )
        .subscribe(
            resItemList => {
              // Log success
              console.log('The following list of items has ' +
                  'successfully been collected');
              console.log(resItemList);
              // Update the local itemList
              this.itemList = resItemList;
              // Emit the subject
              this.emitItemListSubject();
            },
            error1 => console.error(error1)
        );
    return this.itemList;
  }

  deleteItem(itemId: string) {
    this.httpClient
        .delete<ItemModel[]>(`${this.authService.apiUri}/items/${itemId}`,
            {
              headers:
                  {
                    Authorization:
                        `Bearer ${this.authService.getToken()}`
                  }
            })
        // Transform the returning item object into an ItemModel[]
        .pipe(
            map((items) => items.map((itemMap: ItemModel) => new ItemModel(itemMap))),
            catchError(() => throwError(`Item with item_id: ${itemId} not successfully removed`))
        )
        .subscribe(
            newItemList => {
              if (newItemList.some( item => item.itemId === itemId )) {
                console.error('Delete failed');
              } else {
                console.log('The item was successfully removed');
                this.itemList = newItemList;
                // Emit the subject
                this.emitItemListSubject();
              }
            },
            error1 => {
              console.log(error1);
            }
        );
  }
}

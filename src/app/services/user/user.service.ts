import {Injectable} from '@angular/core';
import {UserModel} from '../../models/user.model';
import {HttpClient} from '@angular/common/http';
import {catchError, map} from 'rxjs/operators';
import {AuthService} from '../auth/auth.service';
import {Observable, throwError} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
      private httpClient: HttpClient,
      private authService: AuthService
  ) {
  }

    /**
     * This function will return the id of the connected user
     * registered through his JWT
     */
  getConnectedUserId(): string {
    return this.authService.getUserDetails()._id;
  }

    /**
     * This function use the id of the connected user to retrieve
     * all the user information from the DB.
     * @return An observable of the user
     */
  getConnectedUser(): Observable<UserModel> {
    const userId = this.getConnectedUserId();
    return this.httpClient
        .get<UserModel>(
            `${this.authService.apiUri}/users/${userId}`,
            {
              headers:
                  {
                    Authorization:
                        `Bearer ${this.authService.getToken()}`
                  }
            }
        )
        .pipe(
            map((user: UserModel) => new UserModel(user)),
            catchError((err, caught) => throwError(err))
        );

  }

    /**
     * This function will search and retrieve on the DB the user that owe
     * the given object identified by his itemId
     * @param itemId: The itemId of the object
     * @return An observable of user. The owner of the itemId
     */
  getItemOwner(itemIdHash: string): Observable<UserModel> {
    return this.httpClient
        .get<UserModel>(
            `${this.authService.apiUri}/items/${itemIdHash}/owner`,
        )
        .pipe(
            map(value => new UserModel(value)),
            catchError(() => throwError('ItemOwner not found'))
        );
  }

}

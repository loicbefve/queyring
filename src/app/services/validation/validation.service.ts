import {Injectable} from '@angular/core';
import {AbstractControl, FormGroup, ValidatorFn} from '@angular/forms';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class ValidationService {
  // TODO: Wording séparé / Gestion des langues
  // TODO: Rethink message way, maybe add more generic messages and 1 message/validator except pattern ones

  /**
   * Register
   */
  registerValidationMessage = {
    firstName: [
      {type: 'required', message: 'Votre nom est requis'},
      {type: 'maxLength', message: 'Votre nom ne peut pas dépasser 128 caractères'},
      {type: 'pattern', message: 'Votre nom ne peut contenir que des lettres, des espaces et des "-"'}
    ],
    lastName: [
      {type: 'required', message: 'Votre prénom est requis'},
      {type: 'maxLength', message: 'Votre prénom ne peut pas dépasser 128 caractères'},
      {type: 'pattern', message: 'Votre prénom ne peut contenir que des lettres, des espaces et des "-"'}
    ],
    email: [
      {type: 'required', message: 'L\'email est requis'},
      {type: 'email', message: 'Votre email n\'est pas valide'},
    ],
    phoneNumber: [
      {type: 'required', message: 'Le numéro de téléphone est requis'},
      {type: 'pattern', message: 'Votre numéro de téléphone doit être de la forme 0612345678'}
    ],
    password: [
      {type: 'required', message: 'Le mot de passe est requis'},
      {type: 'pattern', message: 'Votre mot de passe doit contenir au moins 6 charactères'}
    ],
    checkPassword: [
      {type: 'checkPassword', message: 'Les deux mots de passe doivent être identiques'}
    ]
  };

  /**
   * Signin
   */
  signinValidationMessage = {
    email: [
      {type: 'required', message: 'L\'email est requis'},
      {type: 'email', message: 'Votre email n\'est pas valide'},
    ],
    password: [
      {type: 'required', message: 'Le mot de passe est requis'},
      {type: 'pattern', message: 'Votre mot de passe doit contenir au moins 6 charactères'}
    ]
  };

  /**
   * Add item
   */
  addItemValidationMessage = {
    itemId: [
      {type: 'required', message: 'L\'identifiant de l\'objet est obligatoire'},
      {type: 'pattern', message: 'Le format de l\'identifiant de l\'objet est 15 caractères alphanumérique'}
    ],
    itemPass: [
      {type: 'required', message: 'Le mot de passe de l\'objet est obligatoire'},
      {type: 'pattern', message: 'Le format du mot de passe de l\'objet est 2 caractères décimaux'}
    ],
    itemDescription: [
      {type: 'required', message: 'La description de l\'objet est obligatoire'},
      {type: 'maxLength', message: '128 caractères maximum'},
    ]
  };

  /**
   * Finder Form
   */
  finderFormValidationMessage = {
    itemPass: [
      {type: 'required', message: 'Le mot de passe de l\'objet est obligatoire'},
      {type: 'pattern', message: 'Le format du mot de passe de l\'objet est 2 caractères décimaux'}
    ],
    itemDescription: [
      {type: 'required', message: 'La description de l\'objet est requise'},
      {type: 'maxLength', message: 'Maximum 200 caractères'}
    ],
    meetInformation: [
      {type: 'required', message: 'Les informations de rencontre sont requises'},
      {type: 'maxLength', message: 'Maximum 200 caractères'}
    ],
    lastName: [
      {type: 'required', message: 'Votre nom est requis'},
      {type: 'maxLength', message: 'La taille maximale autorisée est de 128 caractères'}
    ],
    firstName: [
      {type: 'required', message: 'Votre prénom est requis'},
      {type: 'maxLength', message: 'La taille maximale autorisée est de 128 caractères'}
    ],
    email: [
      {type: 'required', message: 'Votre email est requis'},
      {type: 'email', message: 'Un email valide est requis'}
    ],
    phone: [
      {type: 'required', message: 'Votre téléphone est requis'},
      {type: 'maxLength', message: 'Votre numéro de téléphone doit contenir 10 caractères'}
    ],
    streetNumber: [
      {type: 'required', message: 'Champs requis'},
      {type: 'maxLength', message: 'Maximum 10 caractères'}
    ],
    street: [
      {type: 'required', message: 'Champs requis'},
      {type: 'maxLength', message: 'Maximum 40 caractères'}
    ],
    streetComplement: [
      {type: 'required', message: 'Champs requis'},
      {type: 'maxLength', message: 'Maximum 50 caractères'}
    ],
    zipCode: [
      {type: 'required', message: 'Champs requis'},
      {type: 'maxLength', message: 'Maximum 10 caractères'}
    ],
    city: [
      {type: 'required', message: 'Champs requis'},
      {type: 'maxLength', message: 'Maximum 50 caractères'}
    ],
    country: [
      {type: 'required', message: 'Champs requis'},
      {type: 'maxLength', message: 'Maximum 80 caractères'}
    ],
    iban: [
      {type: 'required', message: 'L\'IBAN est requis'},
      {type: 'pattern', message: 'Un IBAN français valide est requis'}
    ],
  };

  constructor() {
  }

  checkPasswordValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      try {
        const password1 = control.parent.get('password').value;
        const password2 = control.value;
        return password1 !== password2 ? {checkPassword: {value: control.value}} : null;
      } catch (e) {
        return {value: control.value};
      }


    };
  }


}

import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

export interface UserDetails {
  _id: string;
  email: string;
  firstName: string;
  exp: number;
  iat: number;
}

interface TokenResponse {
  token: string;
}

export interface TokenPayload {
  firstName?: string;
  lastName?: string;
  email: string;
  phoneNumber?: string;
  password: string;
}

@Injectable({
  providedIn: 'root'
})


export class AuthService {

  private token: string;
  public apiUri = 'http://localhost:4000';
  public redirectUrl: string;

  constructor(
      private httpClient: HttpClient,
      private router: Router
  ) {
  }

  /** Useless now that signin and register are reunited */
  // /**
  //  * Guard in order to protect the /auth route
  //  * which has no specific content, it's just
  //  * parent of auth/signin & auth/register
  //  * @param route: The actual route
  //  * @param state: The actual router state
  //  */
  // canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
  //   if (state.url.toString() === '/auth') {
  //     this.router.navigate(['auth', 'signin']);
  //   }
  //   return true;
  // }

  /**
   * Method that will collect the current session token
   */
  public getToken(): string {
    if (!this.token) {
      this.token = localStorage.getItem('mean-token');
    }
    return this.token;
  }

  /**
   * Get the user details from his token
   */
  public getUserDetails(): UserDetails {
    const token = this.getToken();
    let payload;
    if (token) {
      payload = token.split('.')[1].replace(/_/, '/');
      payload = window.atob(payload);
      return JSON.parse(payload);
    } else {
      return null;
    }
  }

  /**
   * Save a token JWT between server and client in the local
   * storage
   * @param token: the string token to save
   */
  private saveToken(token: string): void {
    localStorage.setItem('mean-token', token);
    this.token = token;
  }

  /**
   * Check if user is logged or not
   */
  public isLoggedIn(): boolean {
    const user = this.getUserDetails();
    if (user) {
      return user.exp > Date.now() / 1000;
    } else {
      return false;
    }
  }

  /**
   * Register a user
   * @param user: A tokenPayload containing user information
   */
  public register(user: TokenPayload): Observable<any> {
    return this.httpClient
        .post(`${this.apiUri}/auth/register`, user)
        .pipe(
            map((data: TokenResponse) => {
              if (data.token) {
                this.saveToken(data.token);
              }
              return data;
            })
        );
  }

  /**
   * Log a given user
   * @param user: A tokenPayload containing user information
   */
  public login(user: TokenPayload): Observable<any> {
    return this.httpClient
        .post(`${this.apiUri}/auth/login`, user)
        .pipe(
            map((data: TokenResponse) => {
              if (data.token) {
                this.saveToken(data.token);
              }
              return data;
            })
        );
  }

  /**
   * Log out the current user
   */
  public logout(): void {
    this.token = '';
    window.localStorage.removeItem('mean-token');
    this.router.navigate(['/']);
  }

}

import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  protected apiUri = 'http://localhost:4000';

  constructor(private http: HttpClient) { }

  protected get(relativeUrl: string): Observable<any> {
    return this.http.get(this.apiUri + relativeUrl);
  }



}

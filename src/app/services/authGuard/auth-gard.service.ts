import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {AuthService} from '../auth/auth.service';


@Injectable({
  providedIn: 'root'
})
export class AuthGardService implements CanActivate {

  constructor(
      private authService: AuthService,
      private router: Router
  ) {
  }

  /**
   * Method apply in order to protect a route
   * it will check that the user is authenticated
   * if not it will redirect it into the login page
   * @param route: A snapshot of the activated route datas
   * @param state: A snapshot of the called route datas
   */
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    // Register into the auth service the called route
    this.authService.redirectUrl = state.url;
    if (!this.authService.isLoggedIn()) {
      this.router.navigate(['/auth']);
      return false;
    }
    return true;
  }

}

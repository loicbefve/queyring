import {Injectable} from '@angular/core';
import {TransactionModel} from '../../models/transaction.model';
import {HttpClient} from '@angular/common/http';
import {AuthService} from '../auth/auth.service';
import {Observable, Subject, throwError} from 'rxjs';
import {catchError, map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TransactionsService {

  transactionsList: TransactionModel[];
  transactionsListSubject: Subject<TransactionModel[]> = new Subject<TransactionModel[]>();

  constructor(
      private httpClient: HttpClient,
      private authService: AuthService
  ) {
  }

  emitTransactionsListSubject() {
    this.transactionsListSubject.next(this.transactionsList);
  }

  /**
   * Post a transaction on the db
   * @param transaction: The transaction to post on the db
   */
  postTransaction(transaction: TransactionModel): Observable<TransactionModel> {
    return this.httpClient
        .post<TransactionModel>(
            `${this.authService.apiUri}/transactions`,
            transaction,
            {
              headers:
                  {
                    Authorization:
                        `Bearer ${this.authService.getToken()}`
                  }
            }
        );
  }

  /**
   * Get all the transactions of the database
   */
  getTransactions(): Observable<TransactionModel> {
    return this.httpClient
        .get<TransactionModel>(`${this.authService.apiUri}/transactions`,
            {
              headers:
                  {
                    Authorization:
                        `Bearer ${this.authService.getToken()}`
                  }
            }
        )
        .pipe(
            map((transaction) => new TransactionModel(transaction)),
            catchError((err, caught) => throwError(err))
        );
  }

  getUserTransactions(userId: string): TransactionModel[] {
    this.httpClient
        .get<TransactionModel[]>(`${this.authService.apiUri}/transactions/${userId}`,
            {
              headers:
                  {
                    Authorization:
                        `Bearer ${this.authService.getToken()}`
                  }
            }
        )
        .pipe(
            map((transactions) => transactions.map((transaction: TransactionModel) => new TransactionModel(transaction))),
            catchError((err, caught) => throwError(err))
        )
        .subscribe(
            (transactions: TransactionModel[]) => {
              console.log('The following list of transactions has ' +
                  'successfully been collected');
              console.log(transactions);
              this.transactionsList = transactions;
              this.emitTransactionsListSubject();
            },
            error1 => console.error(error1)
        );

    return this.transactionsList;
  }

  /**
   * Function in order to update the state of a transaction
   * @param transactionId: The id of the concerned transaction
   * @param newState: the wanted new state on 'LOST', 'FIND', 'PAID', 'CONTACT'
   */
  updateState(transactionId: string, newState: string) {
    this.httpClient
        .patch<TransactionModel>(
            `${this.authService.apiUri}/transactions/state/${transactionId}`,
            {state: newState}
        )
        .pipe(
            map((transaction) => new TransactionModel(transaction)),
            catchError((err, caught) => throwError(err))
        )
        .subscribe(
            (transaction: TransactionModel) => {
              console.log('The following transaction has ' +
                  'successfully been updated');
              console.log(transaction);
              // Update the transaction
              console.log(this.transactionsList);
              const transactionPos = this.transactionsList
                  .findIndex((transactionSearch: TransactionModel) => transactionSearch._id === transaction._id);
              this.transactionsList[transactionPos] = transaction;
              console.log(this.transactionsList);
              this.emitTransactionsListSubject();
            },
            error1 => console.error(error1)
        );
  }

}

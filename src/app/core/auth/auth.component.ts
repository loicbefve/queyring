import {Component, OnDestroy, OnInit} from '@angular/core';
import {AuthService} from '../../services/auth/auth.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit, OnDestroy {

  constructor(
      private authService: AuthService,
  ) {
  }

  ngOnInit() {
    console.log('So Im in the auth component');
  }

  ngOnDestroy(): void {
    // If I quit the authentication component
    // I'll clear the auth service redirect url
    console.log('Why am I destroyed???');
    this.authService.redirectUrl = '';
  }

}

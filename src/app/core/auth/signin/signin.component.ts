import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService, TokenPayload} from '../../../services/auth/auth.service';
import {Router} from '@angular/router';
import {ValidationService} from '../../../services/validation/validation.service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {

  signinForm: FormGroup;
  errorMessage: string;

  constructor(
      private formBuilder: FormBuilder,
      private authService: AuthService,
      private router: Router,
      public validationService: ValidationService
  ) {
  }

  ngOnInit() {
    // Initialization of the form
    this.initForm();
  }

  /**
   * Function with the definition of our form
   */
  initForm() {
    this.signinForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  /**
   * When user submit the form
   * we called the login method
   * of the auth service with
   * user inputs
   */
  onSubmit() {

    this.errorMessage = '';

    const credentials: TokenPayload = {
      email: this.signinForm.get('email').value,
      password: this.signinForm.get('password').value
    };
    this.authService.login(credentials)
        .subscribe((value) => {
          console.log(value);
          const redirect = this.authService
              .redirectUrl ? this.router.parseUrl(this.authService.redirectUrl) : '/';
          this.router.navigateByUrl(redirect);
          console.log('logged');
        }, (err) => {

          console.error(err);

          /** Bad credentials */
          if (err.status === 401) {

            this.errorMessage = 'Mauvais email/Mot de passe';

          } else if (err.status === 404) {

            this.errorMessage = 'Le serveur a rencontré un problème, merci de réessayer plus tard';

          } else {

            console.error(err);

          }


        });
  }

}

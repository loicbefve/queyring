import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthService, TokenPayload} from '../../../services/auth/auth.service';
import {Router} from '@angular/router';
import {ValidationService} from '../../../services/validation/validation.service';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;
  errorMessage: string;
  hide1 = true;
  hide2 = true;

  constructor(private formBuilder: FormBuilder,
              private authService: AuthService,
              private router: Router,
              public validationService: ValidationService) {
  }

  ngOnInit() {
    this.initForm();
  }

  /**
   * Init the form with full validationMessage
   */
  initForm() {
    this.registerForm = new FormGroup({
      firstName: new FormControl('',
        [
          Validators.required,
          Validators.maxLength(128),
          Validators.pattern(/^[\D]+$/)
        ]
      ),
      lastName: new FormControl('',
        [
          Validators.required,
          Validators.maxLength(128),
          Validators.pattern(/^[\D]+$/)
        ]),
      email: new FormControl('',
        [
          Validators.required,
          Validators.email
        ]),
      phoneNumber: new FormControl('',
        [
          Validators.required,
          Validators.pattern(/\d{10}/)
        ]),
      password: new FormControl('',
        [
          Validators.required,
          Validators.pattern(/.{6,}/)
        ],
      ),
      checkPassword: new FormControl('',
        {
          validators: [
            this.validationService.checkPasswordValidator()
          ],
          updateOn: 'change'
        })
    }, {updateOn: 'blur'});
  }

  /**
   * When user submit the form, I will
   * call the register method of the
   * auth service with user inputs
   */
  onSubmit() {
    const credentials: TokenPayload = {
      firstName: this.registerForm.get('firstName').value,
      lastName: this.registerForm.get('lastName').value,
      email: this.registerForm.get('email').value,
      phoneNumber: this.registerForm.get('phoneNumber').value,
      password: this.registerForm.get('password').value
    };
    this.authService
      .register(credentials)
      .subscribe((data) => {
        console.log(data); // TODO: Supress it in prod mode
        const redirect = this.authService
            .redirectUrl ? this.router.parseUrl(this.authService.redirectUrl) : '/';
        this.router.navigateByUrl(redirect);
      }, (error) => {
        console.log(error);
        this.errorMessage = error.error;
      });
  }

}

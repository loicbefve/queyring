import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../services/auth/auth.service';
import {Router} from '@angular/router';
import {BreakpointObserver, Breakpoints} from '@angular/cdk/layout';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  host: {class: 'component-container'},
})


export class HomeComponent implements OnInit {


  isDividerVertical = false;

  constructor(
      private authService: AuthService,
      private router: Router,
      private breakpointObserver: BreakpointObserver
  ) {
    breakpointObserver.observe([
      Breakpoints.Medium,
        Breakpoints.Large,
        Breakpoints.XLarge,
    ]).subscribe(result => {
      this.isDividerVertical = result.matches;
    });
  }

  ngOnInit() {
  }

  onFindClick() {
    this.router.navigate(['qr-reader']);
  }

  onRegisterItemClick() {
    this.router.navigate(['add-item']);
  }

}

import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {ActivatedRoute, ActivatedRouteSnapshot, NavigationEnd, Router, RouterEvent} from '@angular/router';
import {Breadcrumb} from '../../shared/custom-types/breadcrumb.type';
import {distinctUntilChanged, filter, map} from 'rxjs/operators';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class BreadcrumbComponent implements OnInit {

  breadcrumbs;

  constructor(
      private router: Router,
      private activatedRoute: ActivatedRoute
  ) {
  }

  ngOnInit() {
    this.breadcrumbs = this.router.events
        .pipe(
            filter( (e: RouterEvent) => e instanceof NavigationEnd),
            map(event =>  this.buildBreadCrumb(this.router.routerState.snapshot.root))
        );
  }

  buildBreadCrumb(route: ActivatedRouteSnapshot, url: string = '',
                  breadcrumbs: Array<Breadcrumb> = []): Array<Breadcrumb> {
    // If no routeConfig is available we are on the root path
    const labelc = route.routeConfig ? route.routeConfig.data.breadcrumb : 'Accueil';
    const path = route.routeConfig ? route.routeConfig.path : '';
    // In the routeConfig the complete path is not available,
    // so we rebuild it each time
    const nextUrl = `${url}${path}/`;
    const breadcrumb = {
      label: labelc,
      url: nextUrl
    };
    const newBreadcrumbs = [...breadcrumbs, breadcrumb];
    if (route.firstChild) {
      // If we are not on our current path yet,
      // there will be more children to look after, to build our breadcrumb
      return this.buildBreadCrumb(route.firstChild, nextUrl, newBreadcrumbs);
    }
    return newBreadcrumbs;
  }

}

import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FooterComponent} from './footer/footer.component';
import {HomeComponent} from './home/home.component';
import {RegisterComponent} from './auth/register/register.component';
import {SigninComponent} from './auth/signin/signin.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AppRoutingModule} from '../app-routing.module';
import { NotFoundComponent } from './not-found/not-found.component';
import { BreadcrumbComponent } from './breadcrumb/breadcrumb.component';
import { AuthComponent } from './auth/auth.component';
import {SharedModule} from '../shared/shared.module';
import {FlexLayoutModule} from '@angular/flex-layout';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NavBarComponent} from './nav-bar/nav-bar.component';

@NgModule({
    declarations: [
        FooterComponent,
        NavBarComponent,
        HomeComponent,
        RegisterComponent,
        SigninComponent,
        AuthComponent,
        NotFoundComponent,
        BreadcrumbComponent,
    ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        AppRoutingModule,
        SharedModule,
        FlexLayoutModule,
        BrowserAnimationsModule
    ],
    exports: [
        FooterComponent,
        NavBarComponent,
        HomeComponent,
        RegisterComponent,
        SigninComponent,
        CommonModule,
        FormsModule
    ]
})
export class CoreModule {
}

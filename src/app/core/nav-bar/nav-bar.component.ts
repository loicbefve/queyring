import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../services/auth/auth.service';
import {Router} from '@angular/router';

@Component({
    selector: 'app-nav-bar',
    templateUrl: './nav-bar.component.html',
    styleUrls: ['./nav-bar.component.scss']
})

export class NavBarComponent implements OnInit {

    constructor(
        public authService: AuthService,
        private router: Router
    ) {
    }

    ngOnInit() {
    }

    onLogoClick() {
        this.router.navigate(['']);
    }

    onPersonalAreaClick() {
        this.router.navigateByUrl('/profile');
    }

    onLogOut() {
        this.authService.logout();
    }
}

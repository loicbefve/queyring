import {Injectable, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ValidationComponent} from './validationMessage/validation.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDividerModule } from '@angular/material/divider';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule, MatIconRegistry } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatStepperModule } from '@angular/material/stepper';
import { MatTabsModule } from '@angular/material/tabs';
import {FlexLayoutModule, FlexStyleBuilder, StyleBuilder, StyleDefinition} from '@angular/flex-layout';
import { ReverseButtonComponent } from './custom-elements/reverse-button/reverse-button.component';
import { BlackButtonComponent } from './custom-elements/black-button/black-button.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {LoadingOrErrorComponent} from './loading-wrapper/loading-or-error.component';
import {MatGridListModule, MatToolbarModule} from '@angular/material';
import { AngularResizedEventModule } from 'angular-resize-event';


@Injectable()
export class CustomFlexStyleBuilder extends StyleBuilder {
  buildStyles(input: string): StyleDefinition {

    console.log(input);

    const cleanedInput = input
        .replace(RegExp('\\s+'), ' ')
        .trim();

    return {
      flex: cleanedInput
    };
  }
}


@NgModule({
  declarations: [
    ValidationComponent,
    ReverseButtonComponent,
    BlackButtonComponent,
    LoadingOrErrorComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatAutocompleteModule,
    MatDatepickerModule,
    MatCardModule,
    MatIconModule,
    MatMenuModule,
    MatDividerModule,
    MatFormFieldModule,
    MatInputModule,
    MatTabsModule,
    MatSidenavModule,
    FlexLayoutModule,
    MatListModule,
    MatStepperModule,
    BrowserAnimationsModule,
    MatExpansionModule,
    MatToolbarModule,
    MatGridListModule,
    AngularResizedEventModule
  ],
  exports: [
    ValidationComponent,
    FormsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatAutocompleteModule,
    MatDatepickerModule,
    MatCardModule,
    MatIconModule,
    MatMenuModule,
    MatDividerModule,
    MatFormFieldModule,
    MatInputModule,
    MatTabsModule,
    MatSidenavModule,
    FlexLayoutModule,
    MatListModule,
    ReverseButtonComponent,
    MatStepperModule,
    BlackButtonComponent,
    MatExpansionModule,
    LoadingOrErrorComponent,
    MatToolbarModule,
    MatGridListModule,
    AngularResizedEventModule
  ],
  providers: [
    {
      provide: FlexStyleBuilder,    // when default is requested
      useClass: CustomFlexStyleBuilder  // provide instead custom builder
    },
    MatIconRegistry
  ]
})
export class SharedModule {
}

import {Component, Input, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {ValidationMessageType} from '../custom-types/validationMessage.type';

@Component({
  selector: 'app-validation',
  templateUrl: './validation.component.html',
  styleUrls: ['./validation.component.scss']
})
export class ValidationComponent implements OnInit {

  @Input() form: FormGroup;
  @Input() dataPath: string[];
  @Input() validationObject: ValidationMessageType;

  constructor() { }

  ngOnInit() {
  }

}

import {Component, Input, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {NgIfContext} from '@angular/common';
import {LoadingWrapper} from './LoadingWrapper';

@Component({
  selector: 'app-loading-or-error',
  templateUrl: './loading-or-error.component.html',
  styleUrls: ['./loading-or-error.component.scss']
})
export class LoadingOrErrorComponent implements OnInit {

  /**
   * The template that should get created when we are in a loading or error state.
   * Use it in the else condition of *ngIf.
   */
  @ViewChild('template', {static: true}) template: TemplateRef<NgIfContext>|null = null;

  /**
   * The loading wrapper that should be used to show the loading/error state
   */
  @Input() loadingWrapper: LoadingWrapper<any>|null = null;

  /**
   * A configurable error message for error cases.
   */
  @Input() errorMessage = 'A error occured!';

  constructor() { }

  ngOnInit() {
  }

}

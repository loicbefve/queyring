import { Observable } from 'rxjs';
import { catchError, shareReplay } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { of } from 'rxjs';
import {NgModule} from '@angular/core';

export class LoadingWrapper<T> {
  readonly data$: Observable<{}|T>;
  private readonly _errorLoading$ = new Subject<boolean>();
  readonly errorLoading$: Observable<boolean> = this._errorLoading$.pipe(
      shareReplay(1)
  );

  constructor(data: Observable<T>) {
    this.data$ = data.pipe(
        shareReplay(1),
        catchError(error => {
          console.error(error);
          this._errorLoading$.next(true);
          return of();
        })
    );
  }

}

import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-black-button',
  templateUrl: './black-button.component.html',
  styleUrls: ['./black-button.component.scss']
})
export class BlackButtonComponent implements OnInit {

  @Input() text: string;

  buttonClass = 'unselected';

  constructor(
      private router: Router
  ) {
  }

  ngOnInit() {
  }

  onMouseEnter() {
    this.buttonClass = 'selected';
  }

  onMouseout() {
    this.buttonClass = 'unselected';
  }

}

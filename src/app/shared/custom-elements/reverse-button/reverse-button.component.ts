import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-reverse-button',
  templateUrl: './reverse-button.component.html',
  styleUrls: ['./reverse-button.component.scss']
})
export class ReverseButtonComponent implements OnInit {

  @Input() text: string;


  buttonClass = 'main-button';


  constructor() {
  }

  ngOnInit() {
  }

  onMouseEnter() {
    this.buttonClass = 'main-button-reverse';
  }

  onMouseout() {
    this.buttonClass = 'main-button';
  }

}

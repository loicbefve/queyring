import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReverseButtonComponent } from './reverse-button.component';

describe('ReverseButtonComponent', () => {
  let component: ReverseButtonComponent;
  let fixture: ComponentFixture<ReverseButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReverseButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReverseButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

export interface UpdateResultType {
  n: number;
  modified: number;
  ok: number;
}

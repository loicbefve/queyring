export type StateType = 'LOST' | 'FOUND' | 'PAID' | 'CONTACT' | 'VALIDATE' | 'CONFLICT';

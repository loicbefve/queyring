export interface ValidationMessageType {
  type: string;
  message: string;
}

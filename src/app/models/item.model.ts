class ItemSchema {

  public itemId: string;
  public idHash?: string;
  public itemPass: string;
  public itemDescription: string;

  constructor(args: ItemSchema) {
    this.itemId = args.itemId;
    this.idHash = args.idHash;
    this.itemPass = args.itemPass;
    this.itemDescription = args.itemDescription;
  }


}

export class ItemModel extends ItemSchema {

  toString(): string {
    return `${this.itemId}\n${this.itemPass}\n${this.itemDescription}`;
  }

  isEmpty(): boolean {
    return this.itemId === undefined &&
        this.idHash === undefined &&
        this.itemPass === undefined &&
        this.itemDescription === undefined;
  }

}

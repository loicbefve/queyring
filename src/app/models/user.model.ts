import {ContactDetailsModel} from './contactDetails.model';
import {BankingDetailsModel} from './bankingDetails.model';
import {ItemModel} from './item.model';

class UserSchema {

    public _id: string;
    public contactDetails?: ContactDetailsModel;
    public role: string;
    public items: ItemModel[];
    public bankingDetails?: BankingDetailsModel;

    constructor(args: UserSchema) {
        this._id = args._id;
        this.contactDetails = new ContactDetailsModel(args.contactDetails);
        this.role = args.role;
        this.items = args.items.map(item => new ItemModel(item));
        this.bankingDetails = new BankingDetailsModel(args.bankingDetails);
    }

}

export class UserModel extends UserSchema {

    isEmpty(): boolean {
        return this._id === undefined &&
            this.contactDetails.isEmpty() &&
            this.role === undefined &&
            this.items.every( item => item.isEmpty() ) &&
            this.bankingDetails.isEmpty();
    }

    toString(): string {
        return `id: ${this._id}\n
        contactDetails: ${this.contactDetails.toString()}\n
        role: ${this.role}\n
        items: ${this.items.map( item => item.toString()).join('\n')}
        bankingDetails: ${this.bankingDetails.toString()}`
        ;

    }

}

import {ContactDetailsModel} from './contactDetails.model';
import {BankingDetailsModel} from './bankingDetails.model';
import {StateType} from '../shared/custom-types/state.type';

class TransactionSchema {

  public _id?: string;
  public finder: ContactDetailsModel;
  public itemDescription: string;
  public meetInformation: string;
  public bankingDetails: BankingDetailsModel;
  public ownerId: string;
  public creationDate?: number;
  public updateDate?: number;
  public state: StateType;

  constructor(args: TransactionSchema) {
    this._id = args._id;
    this.finder = args.finder;
    this.itemDescription = args.itemDescription;
    this.meetInformation = args.meetInformation;
    this.bankingDetails = args.bankingDetails;
    this.ownerId = args.ownerId;
    this.creationDate = args.creationDate;
    this.updateDate = args.updateDate;
    this.state = args.state;
  }
}

export class TransactionModel extends TransactionSchema {

  toString(): string {
    return '';
  }

  isEmpty(): boolean {
    return this._id === undefined &&
        this.finder === undefined &&
        this.itemDescription === undefined &&
        this.meetInformation === undefined &&
        this.bankingDetails.isEmpty() &&
        this.ownerId === undefined &&
        this.creationDate === undefined &&
        this.updateDate === undefined &&
        this.state === undefined;
  }

  getFrenchState(): string {
    return this.state;
  }
}

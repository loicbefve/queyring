class BankingDetailsSchema {

    iban?: string;

    constructor(args: BankingDetailsSchema = {}) {
        this.iban = args.iban;
    }
}

export class BankingDetailsModel extends BankingDetailsSchema {

    isEmpty(): boolean {
        return this.iban === undefined;
    }

    toString(): string {
        return `iban: ${this.iban}`;
    }

}

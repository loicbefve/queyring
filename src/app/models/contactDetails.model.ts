import {AddressModel} from './address.model';

class ContactDetailsSchema {

    public firstName: string;
    public lastName: string;
    public address?: AddressModel;
    public email: string;
    public phoneNumber: string;

    constructor(args: ContactDetailsSchema) {
        this.firstName = args.firstName;
        this.lastName = args.lastName;
        this.address = args.address;
        this.email = args.email;
        this.phoneNumber = args.phoneNumber;
    }
}

export class ContactDetailsModel extends ContactDetailsSchema {

    isEmpty(): boolean {
        return this.firstName === undefined &&
            this.lastName === undefined &&
            this.address.isEmpty() &&
            this.email === undefined &&
            this.phoneNumber === undefined;
    }

    toString(): string {
        return `firstname: ${this.firstName}\n
    lastname: ${this.lastName}\n
    address: ${this.address.toString()}\n
    email: ${this.email}\n
    phoneNumber: ${this.phoneNumber}`;
    }

}

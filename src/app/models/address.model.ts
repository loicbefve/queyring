class AddressSchema {

    public streetNumber: string;
    public street: string;
    public streetComplement: string;
    public zipCode: string;
    public city: string;
    public country: string;

    constructor(args: AddressSchema) {
        this.streetNumber = args.streetNumber;
        this.street = args.street;
        this.streetComplement = args.streetComplement;
        this.zipCode = args.zipCode;
        this.city = args.city;
        this.country = args.country;
    }
}

export class AddressModel extends AddressSchema {

    isEmpty(): boolean {
        return this.streetNumber === undefined &&
            this.street === undefined &&
            this.streetComplement === undefined &&
            this.zipCode === undefined &&
            this.city === undefined &&
            this.country === undefined;
    }

    toString(): string {
        return `streetNumber: ${this.streetNumber}\n
    street: ${this.street}\n
    streetComplement: ${this.streetComplement}\n
    zipCode: ${this.zipCode}\n
    city: ${this.city}\n
    country: ${this.country}`;
    }

}

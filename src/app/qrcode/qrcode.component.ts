import {Component, OnInit} from '@angular/core';

import {Router} from '@angular/router';
import {ResizedEvent} from 'angular-resize-event';


@Component({
    selector: 'app-qrcode',
    templateUrl: './qrcode.component.html',
    styleUrls: ['./qrcode.component.scss'],
    host: {class: 'component-container'}

})
export class QrcodeComponent implements OnInit {

    displayCamera = false;
    hasDevices: boolean;
    hasPermission: boolean;

    constructor(
        private router: Router
    ) {
    }

    ngOnInit() {
    }

    onSimulate() {
        this.router.navigate(
            [
                'found',
                '7a730eaadbcb79af4eee411bd2c566a3033ae6c314f684773fe5ea5716b620fc'
            ]
        );
    }

    /**
     * This function is called when the scan-img on the DOM is resize
     * it will resize the scanner video in consequence.
     * @param $event: Gives the old/new height/width of the scan-img during
     * the resize event
     */
    onScanImageResize($event: ResizedEvent) {

        // The img tag itself (the resize directive worked only on div)
        const realImg = document.getElementById('scan-img');

        // Width of the scanner = width of image times a coefficient
        const width = (realImg.clientWidth * 454 / 629);

        // To get the scanner height we simply apply the 9/16 coefficient
        // cause it's how we build the image (16:9)
        const height = width * 9 / 16;
        const imgTop = realImg.getBoundingClientRect().top;

        // To find the top of the scanner we make the difference between both
        // sizes divided by two cause we centered the image
        const top = imgTop + (realImg.clientHeight - height) / 2;

        // Apply styles
        document.getElementById('scanner')
            .setAttribute(
                `style`,
                `width: ${width.toString()}px;
                 height: ${height.toString()}px;
                  top: ${top.toString()}px`
            );
    }



    onShowCamera() {
        this.displayCamera = true;
    }

    onScanSuccess(result: string) {
        console.log(result);
        this.router.navigateByUrl(result);
    }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { QrcodeRoutingModule } from './qrcode-routing.module';
import { QrcodeComponent } from './qrcode.component';
import {ZXingScannerModule} from '@zxing/ngx-scanner';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FlexLayoutModule} from '@angular/flex-layout';
import {SharedModule} from '../shared/shared.module';
import { AngularResizedEventModule } from 'angular-resize-event';


@NgModule({
  declarations: [QrcodeComponent],
  imports: [
    CommonModule,
    QrcodeRoutingModule,
    ZXingScannerModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
      SharedModule,
      AngularResizedEventModule

  ]
})
export class QrcodeModule { }

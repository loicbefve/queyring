import {Component} from '@angular/core';
import {RouterOutlet} from '@angular/router';
import {animate, animateChild, group, query, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    trigger('routeAnimations', [
      transition('HomePage <=> FinderPage', [
        // First we apply style for routeroutlet
        style({position: 'relative'}),
        // For entering and leaving components also
        query(':enter, :leave', [
          style({
            position: 'absolute',
            top: 0,
            left: 0,
            width: '100%'
          })
        ]),
        // We hide the entering component
        query(':enter', [
          style({left: '-100%'})
        ]),
        // Animate the leaving component child before he go
        query(
            ':leave',
            animateChild()
        ),
        // Make the transition
        group([
          query(
              ':leave',
              [animate('500ms ease-out', style({left: '100%'}))],
              { optional: true },
          ),
          query(
              ':enter',
              [animate('500ms ease-out', style({left: '0%'}))],
              { optional: true }
              )
        ]),
        // Animate the entering component after he went
        query(
            ':enter',
            animateChild(),
            { optional: true }
        ),
      ]),
      transition('* => *', [
        query(
            ':enter',
            style({opacity: 0}),
            { optional: true }
            ),
        query(
            ':enter',
            animate('2s ease-in', style({opacity: 1})),
            { optional: true },
        )
      ])
    ])
  ]
})
export class AppComponent {

  prepareRoute(outlet: RouterOutlet) {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData.animation;
  }
}

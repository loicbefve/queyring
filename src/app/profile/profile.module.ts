import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ProfileRoutingModule} from './profile-routing.module';
import {ProfileComponent} from './profile.component';
import {CoreModule} from '../core/core.module';
import { SideNavbarComponent } from './side-navbar/side-navbar.component';
import { PersonalDatasComponent } from './personal-datas/personal-datas.component';
import {ItemsModule} from '../items/items.module';
import {TransactionsModule} from '../transactions/transactions.module';
import {SharedModule} from '../shared/shared.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    ProfileComponent,
    SideNavbarComponent,
    PersonalDatasComponent
  ],
  imports: [
    CommonModule,
    ProfileRoutingModule,
    CoreModule,
    ItemsModule,
    TransactionsModule,
    SharedModule,
    BrowserAnimationsModule,
    ItemsModule
  ],
})
export class ProfileModule {
}

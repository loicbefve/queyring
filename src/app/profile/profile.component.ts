import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {UserModel} from '../models/user.model';
import {UserService} from '../services/user/user.service';
import {map, shareReplay} from 'rxjs/operators';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
  host: {class: 'component-container'}
})
export class ProfileComponent implements OnInit {

  currentUser$: Observable<UserModel>;
  firstname$: Observable<string>;
  lastname$: Observable<string>;

  constructor(
      private userService: UserService
  ) {
  }

  ngOnInit() {
    this.currentUser$ = this.userService.getConnectedUser().pipe(shareReplay(1));
    this.firstname$ = this.currentUser$.pipe(map( user => user.contactDetails.firstName));
    this.lastname$ = this.currentUser$.pipe(map( user => user.contactDetails.lastName));
  }
}

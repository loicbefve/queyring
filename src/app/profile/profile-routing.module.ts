import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ProfileComponent} from './profile.component';
import {AuthGardService} from '../services/authGuard/auth-gard.service';
import {PersonalDatasComponent} from './personal-datas/personal-datas.component';
import {ItemListComponent} from '../items/itemList/item-list.component';
import {TransactionsListComponent} from '../transactions/transactionsList/transactions-list.component';

const routes: Routes = [
  {path: 'profile',
    component: ProfileComponent,
    canActivate: [AuthGardService],
    data: {
      breadcrumb: 'Profile',
      animation: 'ProfilePage'
    },
    children: [
      {
        path: 'personal-datas',
        component: PersonalDatasComponent,
        data: {
          breadcrumb: 'Données personelles'
        }
      },
      {
        path: 'items',
        component: ItemListComponent,
        data: {
          breadcrumb: 'Objets'
        }
      },
      {
        path: 'transactions',
        component: TransactionsListComponent,
        data: {
          breadcrumb: 'transactions'
        }
      }
    ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProfileRoutingModule {
}

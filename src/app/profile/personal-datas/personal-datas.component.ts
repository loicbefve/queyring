import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../services/auth/auth.service';
import {UserModel} from '../../models/user.model';
import {UserService} from '../../services/user/user.service';
import {Observable} from 'rxjs';
import {map, shareReplay} from 'rxjs/operators';

@Component({
  selector: 'app-personal-datas',
  templateUrl: './personal-datas.component.html',
  styleUrls: ['./personal-datas.component.scss'],
})
export class PersonalDatasComponent implements OnInit {

  currentUser$: Observable<UserModel>;
  email$: Observable<string>;
  firstName$: Observable<string>;
  lastName$: Observable<string>;
  phone$: Observable<string>;
  role$: Observable<string>;


  constructor(
      private authService: AuthService,
      private userService: UserService
  ) {}

  ngOnInit() {
    this.currentUser$ = this.userService.getConnectedUser().pipe(shareReplay(1));
    this.email$ = this.currentUser$.pipe(map( user => user.contactDetails.email));
    this.firstName$ = this.currentUser$.pipe(map(user => user.contactDetails.firstName));
    this.lastName$ = this.currentUser$.pipe(map(user => user.contactDetails.lastName));
    this.phone$ = this.currentUser$.pipe(map( user => user.contactDetails.phoneNumber));
    this.role$ = this.currentUser$.pipe(map( user => user.role));
  }
}

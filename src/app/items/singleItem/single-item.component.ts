import {Component, Input, OnInit} from '@angular/core';
import {ItemModel} from '../../models/item.model';
import {ItemService} from '../../services/item/item.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-item',
  templateUrl: './single-item.component.html',
  styleUrls: ['./single-item.component.scss'],
})
export class SingleItemComponent implements OnInit {

  @Input() item: ItemModel;
  @Input() index: number;

  constructor(
      private itemService: ItemService
  ) {
  }

  ngOnInit() {
  }

  onDelete() {
    this.itemService
        .deleteItem(this.item.itemId);
  }

}

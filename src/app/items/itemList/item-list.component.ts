import {Component, OnDestroy, OnInit} from '@angular/core';
import {ItemModel} from '../../models/item.model';
import {ItemService} from '../../services/item/item.service';
import {Router} from '@angular/router';

@Component({
    selector: 'app-item-list',
    templateUrl: './item-list.component.html',
    styleUrls: ['./item-list.component.scss']
})
export class ItemListComponent implements OnInit, OnDestroy {

    listItems: ItemModel[] = [];
    listItemSubscription = this.itemService
        .itemListSubject
        .subscribe((nextItemList: ItemModel[]) => {
            this.listItems = nextItemList;
        });

    constructor(
        private itemService: ItemService,
        private router: Router
    ) {
    }

    ngOnInit() {
        this.itemService.getItems();
    }

    ngOnDestroy(): void {
        this.listItemSubscription.unsubscribe();
    }

    onAdd() {
        this.router.navigate(['add-item']);
    }

}

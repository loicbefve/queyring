import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ItemsRoutingModule} from './items-routing.module';
import {ItemListComponent} from './itemList/item-list.component';
import {SingleItemComponent} from './singleItem/single-item.component';
import {AddItemComponent} from './addItem/add-item.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SharedModule} from '../shared/shared.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    ItemListComponent,
    SingleItemComponent,
    AddItemComponent
  ],
  imports: [
    CommonModule,
    ItemsRoutingModule,
    ReactiveFormsModule,
    SharedModule,
    BrowserAnimationsModule

  ],
  exports: [
    AddItemComponent,
      ItemListComponent,
    CommonModule,
    FormsModule
  ]
})
export class ItemsModule {
}

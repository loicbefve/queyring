import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {AddItemComponent} from './addItem/add-item.component';
import {AuthGardService} from '../services/authGuard/auth-gard.service';

const routes: Routes = [
  {
    path: 'add-item',
    component: AddItemComponent,
    canActivate: [AuthGardService],
    data: {
      breadcrumb: 'Ajouter un objet'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})

export class ItemsRoutingModule {
}

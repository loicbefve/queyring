import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ValidationService} from '../../services/validation/validation.service';
import {ItemService} from '../../services/item/item.service';
import {UserService} from '../../services/user/user.service';
import {ItemModel} from '../../models/item.model';

@Component({
    selector: 'app-add-item',
    templateUrl: './add-item.component.html',
    styleUrls: ['./add-item.component.scss']
})
export class AddItemComponent implements OnInit {

    addItemForm: FormGroup;
    errorMessage: string;

    constructor(
        private formBuilder: FormBuilder,
        public validationService: ValidationService,
        private itemService: ItemService,
        private userService: UserService,
    ) {
    }

    ngOnInit() {
        this.initForm();
    }

    initForm() {
        this.addItemForm = new FormGroup({
                itemId: new FormControl('',
                    [
                        Validators.required,
                        Validators.pattern(/^[a-z0-9]{15}$/)
                    ]
                ),
                itemPass: new FormControl('',
                    [
                        Validators.required,
                        Validators.pattern(/^[0-9]{2}$/)
                    ]),
                itemDescription: new FormControl('',
                    {
                        validators: [
                            Validators.required,
                            Validators.maxLength(128)
                        ], updateOn: 'change'
                    },
                )
            }, {updateOn: 'blur'}
        );
    }

    onSubmit() {

        const itemToSend = new ItemModel(
            {
                itemId: this.addItemForm.get('itemId').value,
                itemPass: this.addItemForm.get('itemPass').value,
                itemDescription: this.addItemForm.get('itemDescription').value
            }
        );

        console.log(itemToSend.toString());
        this.itemService.postItem(itemToSend);
    }

}

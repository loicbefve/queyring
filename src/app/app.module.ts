import {BrowserModule} from '@angular/platform-browser';
import {Injectable, NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {AuthService} from './services/auth/auth.service';
import {AuthGardService} from './services/authGuard/auth-gard.service';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AppRoutingModule} from './app-routing.module';
import {ItemsModule} from './items/items.module';
import {ProfileModule} from './profile/profile.module';
import {CoreModule} from './core/core.module';
import {ItemsRoutingModule} from './items/items-routing.module';
import {ProfileRoutingModule} from './profile/profile-routing.module';
import {QrcodeModule} from './qrcode/qrcode.module';
import {QrcodeRoutingModule} from './qrcode/qrcode-routing.module';
import {FinderModule} from './finder/finder.module';
import {FinderRoutingModule} from './finder/finder-routing.module';
import {SharedModule} from './shared/shared.module';
import {PaymentRoutingModule} from './payment/payment-routing.module';
import {PaymentModule} from './payment/payment.module';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatCardModule } from '@angular/material/card';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';


@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    // Third party
    BrowserModule,
    FormsModule,
    HttpClientModule,
    SharedModule,
    BrowserAnimationsModule,

    // Routing
    PaymentRoutingModule,
    ItemsRoutingModule,
    ProfileRoutingModule,
    QrcodeRoutingModule,
    FinderRoutingModule,
    AppRoutingModule,
    // My modules
    ItemsModule,
    CoreModule,
    ProfileModule,
    QrcodeModule,
    FinderModule,
    SharedModule,
    PaymentModule,
    MatGridListModule,
    MatCardModule,
    MatMenuModule,
    MatIconModule,
    MatButtonModule,
    LayoutModule,
    MatToolbarModule,
    MatSidenavModule,
    MatListModule,
  ],
  providers: [
    AuthService,
    AuthGardService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

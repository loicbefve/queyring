import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FinderSuccessComponent } from './finder-success.component';

describe('FinderSuccessComponent', () => {
  let component: FinderSuccessComponent;
  let fixture: ComponentFixture<FinderSuccessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FinderSuccessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinderSuccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

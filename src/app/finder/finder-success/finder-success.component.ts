import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-finder-success',
  templateUrl: './finder-success.component.html',
  styleUrls: ['./finder-success.component.scss']
})
export class FinderSuccessComponent implements OnInit {

  constructor(
      private router: Router
  ) {
  }

  ngOnInit() {
  }

  onBackHome() {
    this.router.navigate(['/']);
  }
}

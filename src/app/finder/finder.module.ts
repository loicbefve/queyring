import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {FinderRoutingModule} from './finder-routing.module';
import {FinderComponent} from './finder.component';
import {ReactiveFormsModule} from '@angular/forms';
import {SharedModule} from '../shared/shared.module';
import {FinderSuccessComponent} from './finder-success/finder-success.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    FinderComponent,
    FinderSuccessComponent,
  ],
  imports: [
    CommonModule,
    FinderRoutingModule,
    ReactiveFormsModule,
    SharedModule,
    BrowserAnimationsModule,
  ],
  providers: [
  ]
})
export class FinderModule {
}

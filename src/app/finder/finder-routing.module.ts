import { NgModule } from '@angular/core';
import {Routes, RouterModule, UrlSegment} from '@angular/router';
import {FinderComponent} from './finder.component';
import {FinderSuccessComponent} from './finder-success/finder-success.component';

const routes: Routes = [
  {
    matcher: urlMatch,
    component: FinderComponent,
    data: {
      animation: 'FinderPage',
      breadcrumb: 'J\'ai trouvé'
    }
  },
  {
    path: 'finder-success',
    component: FinderSuccessComponent,
    data: {
      breadcrumb: 'Informations'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FinderRoutingModule { }

export function urlMatch(url: UrlSegment[]) {
  if ( url.length === 2 &&
      url[0].path === 'found' &&
      url[1].path.match(/^[0-9a-z]{64}$/)
  ) {
    return {
      consumed: url,
      posParams: {
        itemIdHash: new UrlSegment(url[1].path, {})
      }
    };
  }
  return null;
}


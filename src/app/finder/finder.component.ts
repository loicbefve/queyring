import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {UserService} from '../services/user/user.service';
import {map} from 'rxjs/operators';
import {TransactionModel} from '../models/transaction.model';
import {ContactDetailsModel} from '../models/contactDetails.model';
import {BankingDetailsModel} from '../models/bankingDetails.model';
import {StateType} from '../shared/custom-types/state.type';
import {TransactionsService} from '../services/transactions/transactions.service';
import {LoadingWrapper} from '../shared/loading-wrapper/LoadingWrapper';

@Component({
  selector: 'app-finder',
  templateUrl: './finder.component.html',
  styleUrls: ['./finder.component.scss'],
  host: {class: 'component-container'}
})
export class FinderComponent implements OnInit {

  itemIdHash: string;

  itemOwnerId$: LoadingWrapper<string>|null;
  itemOwnerId: string;

  // Forms
  meetingGroup: FormGroup;
  finderGroup: FormGroup;
  bankingGroup: FormGroup;


  constructor(
      private router: Router,
      private route: ActivatedRoute,
      private userService: UserService,
      private transactionService: TransactionsService
  ) {
  }

  ngOnInit() {
    this.initForm();
    this.itemIdHash = this.route.snapshot.params.itemIdHash;
    this.itemOwnerId$ = new LoadingWrapper(this.userService
        .getItemOwner(this.itemIdHash)
        .pipe(map(user => user._id)));
    this.itemOwnerId$.data$
        // .pipe(
        //     catchError(err => of(''))
        // )
        .subscribe(
            value => this.itemOwnerId = value as string,
            error1 => console.error(error1),
            () => console.debug('itemOwnerId collected')
        );
  }

  initForm() {


    this.finderGroup = new FormGroup({
      firstName: new FormControl('',
          {
            validators:
                [
                  Validators.required,
                  Validators.maxLength(128)
                ]
          }
      ),
      lastName: new FormControl('',
          {
            validators:
                [
                  Validators.required,
                  Validators.maxLength(128)
                ]
          }
      ),
      email: new FormControl('',
          {
            validators:
                [
                  Validators.required,
                  Validators.email
                ]
          }
      ),
      phone: new FormControl('',
          {
            validators:
                [
                  Validators.required,
                  Validators.maxLength(10)
                ]
          }
      )
    });

    this.meetingGroup = new FormGroup(
        {
          itemDescription: new FormControl('',
              {
                validators:
                    [
                      Validators.required,
                      Validators.maxLength(200)
                    ]
              }
          ),
          meetInformation: new FormControl('',
              {
                validators:
                    [
                      Validators.required,
                      Validators.maxLength(200)
                    ]
              }
          )
        }
    );

    this.bankingGroup = new FormGroup({
      iban: new FormControl('',
          {
            validators: [
              Validators.required,
              Validators.pattern(/^FR[0-9]{10}$/) // TODO: Find real regex
            ]
          }
      )
    });


  }

  /**
   * In order to build a TransactionModel object,
   * we need to provide an object that has the
   * same structure and let the duck typing do.
   * This function is here to do so. It will
   * collect information from form and transform
   * it into a TransactionSchema like object.
   */
  formToTransactionSchema() {
    return {
      finder: new ContactDetailsModel({
        firstName: this.finderGroup.get('firstName').value.toString(),
        lastName: this.finderGroup.get('lastName').value,
        email: this.finderGroup.get('email').value,
        phoneNumber: this.finderGroup.get('phone').value
      }),
      itemDescription: this.meetingGroup.get('itemDescription').value,
      meetInformation: this.meetingGroup.get('meetInformation').value,
      bankingDetails: new BankingDetailsModel({
        iban: this.bankingGroup.get('iban').value
      }),
      ownerId: this.itemOwnerId,
      state: 'FIND' as StateType
    };
  }


  onSubmit() {
    // TODO: If transaction already exists,
    //  just update the state from LOST to FIND
    // Transaction creation

    const transaction = new TransactionModel(
        this.formToTransactionSchema()
    );
    console.log(transaction);
    transaction.state = 'FOUND';
    // Transaction post
    this.transactionService
        .postTransaction(transaction)
        .subscribe(transact => {
            if (transact.isEmpty) {
                console.log('Insert of transaction failed');
            } else {
                console.log('Insert of transaction succeed');
                this.router.navigate(['finder-success']);
            }
        },
            error1 => {
                console.error('Insert of transaction failed');
                console.error(error1);
            }
        );
  }

}

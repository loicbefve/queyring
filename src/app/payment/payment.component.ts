import {Component, OnInit} from '@angular/core';
import {TransactionsService} from '../services/transactions/transactions.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss']
})

export class PaymentComponent implements OnInit {

  constructor(
      private transactionService: TransactionsService,
      private router: Router
  ) { }

  ngOnInit() {
  }

  onPaymentClick() {
    this.transactionService.updateState(history.state._id, 'PAID');
    this.router.navigate(['profile', 'transactions']);
  }

}

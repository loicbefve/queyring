import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {PaymentComponent} from './payment.component';
import {AuthGardService} from '../services/authGuard/auth-gard.service';

const routes: Routes = [
  {
    path: 'payment',
    component: PaymentComponent,
    canActivate: [AuthGardService],
    data: {
      breadcrumb: 'Profile'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PaymentRoutingModule {
}

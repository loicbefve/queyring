import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PaymentComponent} from './payment.component';
import {AuthGardService} from '../services/authGuard/auth-gard.service';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    PaymentComponent
  ],
  imports: [
    CommonModule,
    BrowserAnimationsModule

  ],
  providers: [
  ],
  exports: [
    PaymentComponent
  ]
})
export class PaymentModule {
}

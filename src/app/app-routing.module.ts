import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './core/home/home.component';
import {NotFoundComponent} from './core/not-found/not-found.component';
import {AuthComponent} from './core/auth/auth.component';

const appRoutes: Routes = [
  {
    path: 'auth',
    component: AuthComponent,
    data: {
      animation: 'AuthPage',
      breadcrumb: 'Authentification'
    },
  },
  {
    path: '',
    component: HomeComponent,
    data: {
      animation: 'HomePage',
      breadcrumb: ''
    }
  },
  {
    path: '**',
    component: NotFoundComponent,
    data: {
      label: 'Page inconnue'
    }
  },

];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(appRoutes),
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
